import { Component, h, getAssetPath } from '@stencil/core';

@Component({
  tag: 'main-component',
  styleUrl: 'main-component.css',
  assetsDirs: ['assets'],
  shadow: true,
})
export class MainComponent {
  render() {
    const logoSrc = getAssetPath(`assets/images/logo.svg`);

    return (
      <div>
        <div class="box">
          <div class="logo">
            <img src={logoSrc} />
          </div>
          <form-main />
        </div>
      </div>
    );
  }
}
