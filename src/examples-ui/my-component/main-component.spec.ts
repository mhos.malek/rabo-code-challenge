import { newSpecPage } from '@stencil/core/testing';
import { MainComponent } from './main-component';

describe('main-component', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [MainComponent],
      html: '<main-component></main-component>',
    });
    expect(root).toEqualHtml(`
     <main-component>
       <mock:shadow-root>
         <div>
           <div class="box">
           <div class="logo">
                      <img src="/assets/images/logo.svg">
                    </div>
             <form-main></form-main>
           </div>
         </div>
       </mock:shadow-root>
     </main-component>
    `);
  });
});
