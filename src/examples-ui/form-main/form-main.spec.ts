import { newSpecPage } from '@stencil/core/testing';
import { FormMain } from './form-main';

describe('form-main-input-component', () => {
  it('renders with proper component with money-input inside it ', async () => {
    const { root } = await newSpecPage({
      components: [FormMain],
      html: `<form-main></form-main>`,
    });
    expect(root).toEqualHtml(`
     <form-main>
       <mock:shadow-root>
         <form>
           <form-money-input currency="EURO" defaultvalue="32.3" inputid="sampleOneId" isrequired="" name="withValidationSample"></form-money-input>
           <input class="submit-form-btn"  disabled="" type="submit" value="submit">
         </form>
        </mock:shadow-root>
     </form-main>


    `);
  });
});
