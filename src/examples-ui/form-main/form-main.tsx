import { Component, State, Prop, h } from '@stencil/core';
import { ECurrencies } from '../../types/currencies';

@Component({
  tag: 'form-main',
  styleUrl: 'form-main.css',
  shadow: true,
})
export class FormMain {
  /**
   * all input values
   */
  @State() values: {
    [formItem: string]: string;
  } = {};

  /**
   * all input errors
   */
  @State() errors: {
    [formItem: string]: boolean;
  } = {};

  @State() isValid: boolean = false;

  @State() hasTouched: boolean = false;

  @Prop() shouldConvertLocale: boolean;

  // methods

  updateTouchState() {
    if (!this.hasTouched) this.hasTouched = true;
  }
  onValueChange(name, value) {
    this.updateTouchState();
    this.values = {
      ...this.values,
      [name]: this.shouldConvertLocale ? value.toLocaleString() : value,
    };
  }

  updateFormValidationState() {
    if (Object.values(this.errors).some(key => key === false)) {
      return (this.isValid = false);
    }
    this.isValid = true;
  }

  handleValidationStateChange(name: string, validationState: boolean) {
    this.errors = {
      ...this.errors,
      [name]: validationState,
    };
    this.updateFormValidationState();
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.isValid) {
      console.log('form submited', this.values);
      // e.target.submit();
    } else {
      // handle form not valid state
    }
  }

  inputs = [
    {
      id: 'sampleOneId',
      currency: ECurrencies.EURO,
      name: 'withValidationSample',
      defaultValue: '32.3',
      customValidations: [],
    },
  ];

  render() {
    return (
      <form onSubmit={event => this.handleSubmit(event)}>
        {this.inputs.map(item => {
          return (
            <form-money-input
              inputID={item.id}
              currency={item.currency}
              name={item.name}
              defaultValue={item.defaultValue}
              onValueChange={(name, value) => this.onValueChange(name, value)}
              customValidations={item.customValidations}
              isRequired={true}
              onValidationStateChange={(name, validationState) => this.handleValidationStateChange(name, validationState)}
            ></form-money-input>
          );
        })}

        {this.hasTouched && (
          <p class="validation-state">
            Form validation status: <span class={this.isValid ? 'validState' : 'inValidState'}>{this.isValid ? 'valid' : 'Not valid'}</span>
          </p>
        )}
        <input type="submit" value="submit" class="submit-form-btn" disabled={!this.isValid} />
      </form>
    );
  }
}
