import { createDecimalNumber } from './utils';

describe('createDecimalNumber', () => {
  it('returns empty string for no names defined', () => {
    expect(createDecimalNumber(undefined, undefined)).toEqual('');
  });

  it('return number with proper format when no decinal passed', () => {
    expect(createDecimalNumber('22', undefined)).toEqual(22.00);
  });

  it('return number with proper format when no fixed number passed', () => {
    expect(createDecimalNumber(undefined, '2')).toEqual(0.2);
  });
});
