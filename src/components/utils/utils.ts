export const createDecimalNumber = (fixValue: string, decimalValue: string = '') => {
  if (!fixValue && !decimalValue) return '';
  return Number(`${fixValue ? fixValue : '0'}${decimalValue ? `.${decimalValue}` : ''}`);
};
