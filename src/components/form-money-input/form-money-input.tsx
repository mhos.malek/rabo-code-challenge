import { Component, Prop, h, Fragment, State, Watch } from '@stencil/core';
import { ECurrencies } from '../../types/currencies';
import validator, { TValidationObjectTypes } from '../../utils/validations/validator';
import { createDecimalNumber } from '../utils/utils';

enum INPUT_NAMES {
  FIX = 'inputFix',
  DECIMAL = 'inputDecimal',
}

// TODO > move some validation to util files
@Component({
  tag: 'form-money-input',
  styleUrl: 'form-money-input.css',
  shadow: true,
})
export class FormMoneyInput {
  inputSeperator: ',';

  /**
   * set default or current value if it's needed from parent
   */
  @Prop() defaultValue: string;

  /**
   * set input to disabled
   */

  @Prop() disabled: boolean;

  /**
   * set default or current value if it's needed from parent
   */
  @Prop() onValidationStateChange: (name: string, value: boolean) => void;

  /**
   * pass value to parent on Change
   */
  @Prop() onValueChange!: (name: string, value: string) => void;

  /**
   * input name
   */
  @Prop() name!: string;

  /**
   * internal value of fixed part
   */

  @Prop() customValidations: Array<TValidationObjectTypes>;

  @Prop() currency: ECurrencies;

  @Prop() inputID: string;

  @Prop() max: string;

  @Prop() min: string;

  @Prop() isRequired: boolean;

  // states

  @State() fixValue: string;

  @State() decimalValue: string = '';

  @State() validationState: boolean;

  @State() value: string;

  @State() hasTouched: boolean = false;

  /**
   * set focus to desired input
   */
  @State() currentFocusedElement: INPUT_NAMES = INPUT_NAMES.FIX;

  @Watch('validationState')
  handleValidationStateChange(newState, oldState) {
    if (newState !== oldState) {
      this.onValidationStateChange(this.name, newState);
    }
  }

  // method
  checkValidationState(finalValue: string, HTMLValidity) {
    let isValid = true;

    // if user added custom validator
    if (this.needCustomValidation()) {
      this.customValidations.forEach(validationRule => {
        if (!validator(finalValue, [validationRule.rule], validationRule.param)) {
          isValid = false;
        }
      });
    }

    // use HTML5 Validation
    if (!HTMLValidity.valid) {
      isValid = false;
    }

    this.validationState = isValid;
  }

  needCustomValidation() {
    return this.customValidations?.length > 0;
  }

  updateFinalValue() {
    if (!this.hasTouched) {
      this.hasTouched = true;
    }

    const value = createDecimalNumber(this.fixValue, this.decimalValue);
    // pass to parent

    this.onValueChange(this.name, value as string);
  }

  handleCommaInputOnFixedNumberInput(event) {
    const { keyCode } = event;
    // when user enter inputSeperator => ','
    console.log('keyCode', keyCode);
    if (keyCode === 188) {
      this.currentFocusedElement = INPUT_NAMES.DECIMAL;
    }
  }

  handleCommaAndBackSpaceInputOnDecimalNumberInput(event) {
    const {
      keyCode,
      target: { value, selectionStart },
    } = event;
    // backspace key is pressed
    if (keyCode === 8 && value === '') {
      this.currentFocusedElement = INPUT_NAMES.FIX;
      return;
    }

    // if all numbers being removed
    if (value === '') {
      this.currentFocusedElement = INPUT_NAMES.FIX;
      return;
    }

    // TODO: fix when user press arrow right to move focus
    // Left key is pressed."
    if (keyCode === 37 && selectionStart) {
      this.currentFocusedElement = INPUT_NAMES.FIX;
    }
  }

  handleFixValueChange(event) {
    const {
      target: { value, validity },
    } = event;

    this.fixValue = value;

    // handle change of focus on entering ','
    this.handleCommaInputOnFixedNumberInput(event);
    this.updateFinalValue();

    this.checkValidationState(value, validity);
  }

  handleDecimalValueChange(event) {
    const {
      target: { value, validity },
    } = event;
    this.decimalValue = value;

    // handle change of focus to fixed element on clearing all numbers
    this.handleCommaAndBackSpaceInputOnDecimalNumberInput(event);

    this.updateFinalValue();
    this.checkValidationState(value, validity);
  }

  renderCurrencySign() {
    switch (this.currency) {
      case ECurrencies.EURO:
        return '€';
      case ECurrencies.DOLLAR:
        return '$';
      default:
        break;
    }
  }

  render() {
    return (
      <Fragment>
        <span class="currency-char">{this.renderCurrencySign()}</span>
        <form-fieldset>
          <form-number-input
            inputID={this.inputID}
            onInput={event => this.handleFixValueChange(event)}
            required={this.isRequired}
            name={INPUT_NAMES.FIX}
            value={this.fixValue}
            disabled={this.disabled}
            min="0"
            hasError={!this.validationState && this.hasTouched}
            placeHolder="0,00"
            isFocused={this.currentFocusedElement === INPUT_NAMES.FIX}
          />

          {this.currentFocusedElement === INPUT_NAMES.DECIMAL && <span class="seperator">,</span>}

          <form-number-input
            inputID={this.inputID}
            disabled={this.disabled}
            max="99"
            min="0"
            maxlength="2"
            placeHolder=""
            onInput={event => this.handleDecimalValueChange(event)}
            name={INPUT_NAMES.DECIMAL}
            value={this.decimalValue}
            hasError={!this.validationState && this.hasTouched}
            isFocused={this.currentFocusedElement === INPUT_NAMES.DECIMAL}
          />
        </form-fieldset>
      </Fragment>
    );
  }
}
