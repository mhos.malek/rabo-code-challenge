import { newSpecPage } from '@stencil/core/testing';
import { ECurrencies } from '../../types/currencies';
import { FormMoneyInput } from './form-money-input';

describe('form-money-input-component', () => {
  it('renders with proper currency ', async () => {
    const { root } = await newSpecPage({
      components: [FormMoneyInput],
      html: `<form-money-input currency=${ECurrencies.DOLLAR} ></form-money-input>`,
    });
    expect(root).toEqualHtml(`
     <form-money-input currency="DOLLAR">
       <mock:shadow-root>
         <span class="currency-char">
           $
         </span>
         <form-fieldset>
        <form-number-input isfocused="" min="0" name="inputFix" placeholder="0,00"></form-number-input>
        <form-number-input max="99" maxlength="2" min="0" name="inputDecimal" placeholder="" value=""></form-number-input>
         </form-fieldset>
       </mock:shadow-root>
     </form-money-input>
    `);
  });

  it('renders with proper disable status ', async () => {
    const { root } = await newSpecPage({
      components: [FormMoneyInput],
      html: `<form-money-input currency=${ECurrencies.DOLLAR} disabled={this.disabled}></form-money-input>`,
    });
    expect(root).toEqualHtml(`
     <form-money-input currency="DOLLAR" disabled={this.disabled}>
       <mock:shadow-root>
         <span class="currency-char">
           $
         </span>
         <form-fieldset>
           <form-number-input disabled="" isfocused="" min="0" name="inputFix" placeholder="0,00"></form-number-input>
          <form-number-input disabled="" max="99" maxlength="2" min="0" name="inputDecimal" placeholder="" value=""></form-number-input>
         </form-fieldset>
       </mock:shadow-root>
     </form-money-input>
    `);
  });
});
