import { Component, h } from '@stencil/core';


/*
TODO:
This component has no logic at the moment. but it is here to seperate the concerns and UI in  general
 and it can also do more but for sake of time, I just leave it as it is.
*/
@Component({
  tag: 'form-fieldset',
  styleUrl: 'form-fieldset.css',
  shadow: true,
})
export class FormFieldset {
  render() {
    return (
      <fieldset>
        <slot></slot>
      </fieldset>
    );
  }
}
