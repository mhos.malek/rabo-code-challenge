import { Component, Prop, Watch, h, Fragment } from '@stencil/core';
import isNumeric from '../../utils/validations/validator-functions/number-validator';

@Component({
  tag: 'form-number-input',
  styleUrl: 'form-number-input.css',
  shadow: true,
})
export class FormNumberInput {
  numberInput!: HTMLInputElement;

  /**
   * input name property
   */
  @Prop() name!: string;

  /**
   * input value property
   */
  @Prop() value!: string;

  /**
   * input value property
   */
  @Prop() onInput: (event) => void;

  /**
   * input max value property
   */
  @Prop() max: string;

  /**
   * input min value property
   */
  @Prop() min: string;

  /**
   * input required property
   */
  @Prop() required: boolean;

  /**
   * input readOnly property
   */
  @Prop() readOnly: boolean;

  /**
   * input readOnly property
   */
  @Prop() disabled: boolean;

  /**
   * input readOnly property
   */
  @Prop() inputID!: string;

  /**
   *  is input focused for handle focus from outside
   */
  @Prop() isFocused: boolean;

  @Prop() hasError: boolean;

  @Prop() placeHolder: string;

  @Prop() maxlength: string;

  componentDidLoad() {
    this.resizeInput();
  }

  @Watch('isFocused')
  onInputFocused() {
    if (this.isFocused) {
      this.numberInput.focus();
    }
  }

  resizeInput() {
    const value = this.numberInput?.value;

    if (!value) {
      this.numberInput.style.width = `${String(this.numberInput.placeholder.length)}ch`;
    }
    if (!isNumeric(value)) {
      return null;
    }
    return (this.numberInput.style.width = `${String(this.numberInput.value.length)}ch`);
  }

  handleOnInput(event) {
    this.onInput(event);
    this.resizeInput();
  }

  render() {
    return (
      <Fragment>
        <label htmlFor={this.inputID}></label>
        <input
          type="number"
          placeholder={this.placeHolder}
          disabled={this.disabled}
          max={this.max}
          min={this.min}
          maxlength={this.maxlength}
          required={this.required}
          readOnly={this.readOnly}
          onKeyUp={event => this.handleOnInput(event)}
          name={this.name}
          // value={this.value}
          pattern="[0-9]*"
          inputmode="numeric"
          aria-describedby="hint"
          id={this.inputID}
          class={this.hasError ? 'hasValidationError' : ''}
          ref={el => (this.numberInput = el as HTMLInputElement)}
        />
      </Fragment>
    );
  }
}
