import { newSpecPage } from '@stencil/core/testing';
import { FormNumberInput } from './form-number-input';

describe('form-number-input-component', () => {
  it('renders with proper component with label', async () => {
    const { root } = await newSpecPage({
      components: [FormNumberInput],
      html: `<form-number-input></form-number-input>`,
    });
    expect(root).toEqualHtml(`
    <form-number-input>
       <mock:shadow-root>
         <label></label>
         <input aria-describedby="hint" style="width: 0ch;" inputmode="numeric" pattern="[0-9]*" type="number">
        </mock:shadow-root>
     </form-number-input>
    `);
  });

  it('shoud have proper name ', async () => {
    const MOCK_NAME = 'sampleMockName';
    const { root } = await newSpecPage({
      components: [FormNumberInput],
      html: `<form-number-input name=${MOCK_NAME}></form-number-input>`,
    });
    expect(root).toEqualHtml(`
    <form-number-input name=${MOCK_NAME}>
       <mock:shadow-root>
         <label></label>
         <input name="${MOCK_NAME}" aria-describedby="hint" inputmode="numeric" pattern="[0-9]*" type="number"  style="width: 0ch;">
        </mock:shadow-root>
     </form-number-input>
    `);
  });
  it('shoud have proper max ', async () => {
    const MOCK_MAX = '10';
    const { root } = await newSpecPage({
      components: [FormNumberInput],
      html: `<form-number-input max=${MOCK_MAX}></form-number-input>`,
    });
    expect(root).toEqualHtml(`
    <form-number-input max=${MOCK_MAX}>
       <mock:shadow-root>
         <label></label>
         <input max="${MOCK_MAX}" aria-describedby="hint" inputmode="numeric" pattern="[0-9]*" type="number"  style="width: 0ch;">
        </mock:shadow-root>
     </form-number-input>
    `);
  });

  it('shoud have proper min ', async () => {
    const MOCK_MIN = '5';
    const { root } = await newSpecPage({
      components: [FormNumberInput],
      html: `<form-number-input min=${MOCK_MIN}></form-number-input>`,
    });
    expect(root).toEqualHtml(`
    <form-number-input min=${MOCK_MIN}>
       <mock:shadow-root>
         <label></label>
         <input min="${MOCK_MIN}" aria-describedby="hint" inputmode="numeric" pattern="[0-9]*" type="number"  style="width: 0ch;">
        </mock:shadow-root>
     </form-number-input>
    `);
  });

  it('shoud have proper min ', async () => {
    const MOCK_MIN = '5';
    const { root } = await newSpecPage({
      components: [FormNumberInput],
      html: `<form-number-input min=${MOCK_MIN}></form-number-input>`,
    });
    expect(root).toEqualHtml(`
    <form-number-input min=${MOCK_MIN}>
       <mock:shadow-root>
         <label></label>
         <input min="${MOCK_MIN}" aria-describedby="hint" inputmode="numeric" pattern="[0-9]*" type="number"  style="width: 0ch;">
        </mock:shadow-root>
     </form-number-input>
    `);
  });

  it('shoud have proper placeholder', async () => {
    const MOCK_PLACEHOLDER = 'MOCK_PLACEHOLDER';
    const { root } = await newSpecPage({
      components: [FormNumberInput],
      html: `<form-number-input placeHolder="${MOCK_PLACEHOLDER}"></form-number-input>`,
    });
    expect(root).toEqualHtml(`
    <form-number-input  placeHolder="${MOCK_PLACEHOLDER}">
       <mock:shadow-root>
         <label></label>
         <input aria-describedby="hint" inputmode="numeric" pattern="[0-9]*" type="number"  style="width: 0ch;">
        </mock:shadow-root>
     </form-number-input>
    `);
  });
});
