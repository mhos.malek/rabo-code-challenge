import isNumeric from './validator-functions/number-validator';
import isEmpty from './validator-functions/required-validator';

enum validationTypes {
  customPattern = 'customPattern',
  isRequired = 'isRequired',
  maxValue = 'maxLength',
  minValue = 'minLength',
  onlyNumber = 'onlyNumber',
}

type TValidationObjectTypes = {
  rule: validationTypes;
  param?: string;
};

const validatorFunctions = {
  [validationTypes.onlyNumber]: value => {
    return isNumeric(value);
  },
  [validationTypes.isRequired]: value => {
    return !isEmpty(value);
  },

  [validationTypes.customPattern]: (value, pattern) => {
    if (!pattern.test(value)) {
      return false;
    }
    return true;
  },
};

const validator = (value: string, validatonTypes: validationTypes[], parameter?: string): boolean => {
  let isValid = true;
  validatonTypes.forEach(validatorFn => {
    if (!validatorFunctions[validatorFn](value, parameter)) {
      isValid = false;
    }
  });
  return isValid;
};

const getHTMLValidationsFromValidationRules = validationsRules => {
  let htmlValidations = {
    isRequired: false,
    max: null,
    min: null,
  };
  validationsRules.forEach(validationRule => {
    switch (validationRule.rule) {
      case validationTypes.isRequired:
        htmlValidations = {
          ...htmlValidations,
          isRequired: true,
        };

      case validationTypes.maxValue:
        htmlValidations = {
          ...htmlValidations,
          max: validationRule.param,
        };

      case validationTypes.minValue:
        htmlValidations = {
          ...htmlValidations,
          min: validationRule.param,
        };

      default:
        break;
    }
  });
  return htmlValidations;
};

export { validationTypes, TValidationObjectTypes, getHTMLValidationsFromValidationRules };
export default validator;
