const maxValueValidator = (value: string, parameter: string) => {
  if (Number(value) > Number(parameter)) {
    return false;
  }
  return true;
};

const minValueValidator = (value: string, parameter: string) => {
  if (Number(value) < Number(parameter)) {
    return false;
  }
  return true;
};

export { minValueValidator, maxValueValidator };
