const isEmpty = (str: string) => {
  return !str || str.length === 0;
};
export default isEmpty;
