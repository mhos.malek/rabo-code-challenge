const isNumeric = (str: string) => {
  if (typeof str !== 'string') return false; // we only process strings!
  return !isNaN(parseFloat(str));
};

export default isNumeric;
