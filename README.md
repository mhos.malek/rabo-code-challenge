# Stencil

This project is a code challenge for Rabobank. see the instruction to see how you can build and run the project.

## Getting Started

to start dev server:

```bash
npm install
npm start
```

To build the component for production, run:

```bash
npm run build
```

To run the unit tests, run:

```bash
npm test
```

## Folder structure

A quick look at the files and directories you'll see in the current project.

    ├─── dist (build folder)
    ├─── .gitignore
    ├─── package.json
    ├─── src
    ├──── index.ts (main app module)
    ├──── index.html (main html file)
    ├──── utils (general utils)
    ├──── components (a base folder to re-usable components)
    ├────── <form-fieldset> a fieldset wrapper for money input fields
    ├────── <form-input> input component (here I only needed number so it's only small and dumb input and it's not support other types)
    ├────── <form-money-input> money component render two inputs to show money seperator and validate inputs and ...
    ├────── utils (general utils for convert forms values for money-input)
    ├──── examples-ui (a base folder to showing the examples)
    ├────── <form-main> main component to render forms
    ├────── <my-component> example ui that have a box and logo to render form inside
    ├─── prettier.json (prettier config)
    └─── README.md

## Checklist

This is the checklist of what I have done or what I would do if I had more time.

### Basics

- [x] create a base example for showing the main money-input components
- [x] seperate components to have more control over logic
- [x] add basic unit test
- [x] add basic designs
- [x] unit tests

### doc

- [x] General document

### tech-debt

- [ ] improve Validation
- [ ] E2E test
- [ ] improve localization (not supportint RTL here or thousands seperator for inputs)
- [ ] improve css structure (make variables, make dynamic themes and colors / add css releated mixins or functions to remove in-consistency and more structrable CSS code)
- [ ] fix warnings
- [ ] fix bug when you swith between inputs with mouse and remove all the inputs
- [ ] fix bug when you move cursor with arrow left key on decimal input (it should autofocus to fixed number input)
- [ ] Responsive && UX 

